import React, { Component } from 'react';
import { createStore } from 'redux';
import { bindAll } from 'lodash';
import { isAlpha, isNumeric, isEmail } from 'validator';

const store = createStore((state, action) => {
  if (action.type === 'form') {
    return { ...state, ...action.data }
  }

  return state;
}, {});

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name.replace(/\s+/g, '') === '' ? {
        inputValue: '',
        valid: undefined,
        message: ''
      } : this.validateName(props.name),
      inn: props.inn.replace(/\s+/g, '') === '' ? {
        inputValue: '',
        valid: undefined,
        message: ''
      } : this.validateInn(props.inn),
      email: props.email.replace(/\s+/g, '') === '' ? {
        inputValue: '',
        valid: undefined,
        message: ''
      } : this.validateEmail(props.email),
      date: props.date.replace(/\s+/g, '') === '' ? {
        inputValue: '',
        valid: undefined,
        message: ''
      } : this.validateDate(props.date),
      text: props.text.replace(/\s+/g, '') === '' ? {
        inputValue: '',
        valid: undefined,
        message: ''
      } : this.validateText(props.text)
    };

    store.dispatch({
      type: 'form',
      data: this.setStoreState()
    });

    bindAll(this, ['onNameChange', 'onInnChange', 'onEmailChange', 'onDateChange', 'onTextChange', 'onFormSave']);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    store.dispatch({
      type: 'form',
      data: this.setStoreState()
    });
  }

  setStoreState() {
    return {
      name: this.state.name.valid ? this.state.name.inputValue : '',
      inn: this.state.inn.valid ? this.state.inn.inputValue : '',
      email: this.state.email.valid ? this.state.email.inputValue : '',
      date: this.state.date.valid ? this.state.date.inputValue : '',
      text: this.state.text.valid ? this.state.text.inputValue : ''
    };
  }

  validateName(value) {
    let object = {
      inputValue: value,
      valid: true,
      message: ''
    };

    if (value.replace(/\s+/g, '') === '') {
      object.valid = false;
      object.message = 'Поле обязательно для заполнения';
      return object;
    }

    if (!isAlpha(value.replace(/\s+/g, ''), 'ru-RU')) {
      object.valid = false;
      object.message = 'Поле должно содержать только русские буквы';
      return object;
    }

    return object;
  }

  validateInn(value) {
    let object = {
      inputValue: value,
      valid: true,
      message: ''
    };

    if (value.replace(/\s+/g, '') === '') {
      object.valid = false;
      object.message = 'Поле обязательно для заполнения';
      return object;
    }

    if (!isNumeric(value)) {
      object.valid = false;
      object.message = 'Поле должно содержать только цифры';
      return object;
    }

    return object;
  }

  validateEmail(value) {
    let object = {
      inputValue: value,
      valid: true,
      message: ''
    };

    if (value.replace(/\s+/g, '') === '') {
      object.valid = false;
      object.message = 'Поле обязательно для заполнения';
      return object;
    }

    if (!isEmail(value)) {
      object.valid = false;
      object.message = 'Введите корректный email адрес';
      return object;
    }

    return object;
  }

  validateDate(value) {
    let object = {
      inputValue: value,
      valid: true,
      message: ''
    };

    if (value.replace(/\s+/g, '') === '') {
      object.valid = false;
      object.message = 'Поле обязательно для заполнения';
      return object;
    }

    if (!value.match(/^(?:(?:31(-|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(-|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(-|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(-|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{4})$/)) {
      object.valid = false;
      object.message = 'Дата введена неверно. Введите дату в формате ДД-ММ-ГГГГ';
      return object;
    }

    return object;
  }

  validateText(value) {
    let object = {
      inputValue: value,
      valid: true,
      message: ''
    };

    if (value.replace(/\s+/g, '') === '') {
      object.valid = false;
      object.message = 'Поле обязательно для заполнения';
      return object;
    }

    return object;
  }

  onNameChange(event) {
    this.setState({
      name: this.validateName(event.target.value)
    });
  }

  onInnChange(event) {
    this.setState({
      inn: this.validateInn(event.target.value)
    });
  }

  onEmailChange(event) {
    this.setState({
      email: this.validateEmail(event.target.value)
    });
  }

  onDateChange(event) {
    this.setState({
      date: this.validateDate(event.target.value)
    });
  }

  onTextChange(event) {
    this.setState({
      text: this.validateText(event.target.value)
    });
  }

  onFormSave(event) {
    event.preventDefault();

    if (localStorage) {
      localStorage.setItem('form', JSON.stringify(store.getState()));
    }
  }

  validateForm() {
    if (this.state.name.valid && this.state.date.valid && this.state.inn.valid && this.state.email.valid && this.state.text.valid) {
      return true;
    } else {
      return false;
    }
  }

  setClassName(stateProp) {
    switch (this.state[stateProp].valid) {
      case true:
        return ' valid';
      case false:
        return ' error';
      default:
        return '';
    }
  }

  showMessage(stateProp) {
    return this.state[stateProp].valid === false && <div className="message error-message">{this.state[stateProp].message}</div>;
  }

  render() {
    return (
      <div className="form-wrap">
        <div className="form-title">Форма</div>
        <form className="form">
          <div className="form__item-wrap">
            <input type="text" className={`form__item form__item-input input${this.setClassName('name')}`} 
            placeholder="Ф.И.О" value={this.state.name.inputValue} onChange={this.onNameChange} />
            {this.showMessage('name')}
          </div>
          <div className="form__item-wrap">
            <input type="text" className={`form__item form__item-input input${this.setClassName('date')}`} 
            placeholder="Дата рождения" value={this.state.date.inputValue} onChange={this.onDateChange} />
            {this.showMessage('date')}
          </div>
          <div className="form__item-wrap">
            <input type="text" className={`form__item form__item-input input${this.setClassName('inn')}`} 
            placeholder="ИНН" value={this.state.inn.inputValue} onChange={this.onInnChange} />
            {this.showMessage('inn')}
          </div>
          <div className="form__item-wrap">
            <input type="text" className={`form__item form__item-input input${this.setClassName('email')}`} 
            placeholder="Email" value={this.state.email.inputValue} onChange={this.onEmailChange} />
            {this.showMessage('email')}
          </div>
          <div className="form__item-wrap">
            <textarea className={`form__item form__item-textarea textarea${this.setClassName('text')}`} 
            placeholder="Текст" value={this.state.text.inputValue} onChange={this.onTextChange}></textarea>
            {this.showMessage('text')}
          </div>
          <div className="form__item-wrap form__item-btn-wrap">
            <button className="btn" disabled={this.validateForm() ? false : true} onClick={this.onFormSave}>Сохранить</button>
          </div>
        </form>
      </div>
    );
  }
}

export { Form, store };
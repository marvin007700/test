import React, { Component } from 'react';
import { store } from './Form';

class Document extends Component {
  constructor(props) {
    super(props);

    this.state = store.getState();

    store.subscribe(() => {
      this.setState(store.getState());
    });
  }

  render() {
    return (
      <div className="document-wrap">
        <div className="document-title">Документ</div>
        <div className="document">
          <div className="document__header">
            <div className="document__header-main">
              <div className="document__header-main-item">
                Директору ООО "Компания"
              </div>
            </div>
          </div>
          <div className="document__body">
            <div className="document__body-item">Я,<span className="document__body-item-underline document__body-item-output">{this.state.name}</span></div>
            <div className="document__body-item">Дата рождения<span className="document__body-item-underline document__body-item-output">{this.state.date}</span></div>
            <div className="document__body-item">ИНН<span className="document__body-item-underline document__body-item-output">{this.state.inn}</span></div>
            <div className="document__body-item">email<span className="document__body-item-underline document__body-item-output">{this.state.email}</span></div>
            <div className="document__body-title">Заявление</div>
            <div className="document__body-item-output">{this.state.text}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Document;
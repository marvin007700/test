import React, { Component } from 'react';
import { Form } from './Form';
import Document from './Document';

class App extends Component {
  constructor() {
    super();

    this.state = localStorage.getItem('form') ? JSON.parse(localStorage.getItem('form')) : {
      name: '',
      inn: '',
      email: '',
      date: '',
      text: ''
    };
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <Form name={this.state.name} inn={this.state.inn} email={this.state.email} date={this.state.date} text={this.state.text} />
          <Document />
        </div>
      </div>
    );
  }
}

export default App;